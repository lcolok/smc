module.exports = {
	parts: {
		major: 1,
		minor: 83,
		build: 1,
		revision: 0
	},
	whole: "1.83.1(492)",
	commits: 492
}